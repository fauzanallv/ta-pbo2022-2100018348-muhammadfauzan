public class Pengunjung {
    String nama_pengunjung;
    String no_hp;
    String alamat;

    public String getNama_pengunjung() {
        return nama_pengunjung;
    }

    public void setNama_penyewa(String nama_pengunjung) {
        this.nama_pengunjung = nama_pengunjung;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}